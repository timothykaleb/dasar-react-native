import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native'
const styles = StyleSheet.create({
  container: {
    height: 250,
    width: '75%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: "25%",
    marginLeft: "12.5%",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginRight: "25%",
    elevation: 10,
  },
  container2: {
    height: 180,
    width: '75%',
    backgroundColor: 'white',
    // justifyContent: 'center',
    alignItems: 'center',
    marginLeft: "12.5%",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    elevation: 10,
  },
  logo: {
    width: "90%",
    height: "90%",
  },
  textJudul: {
   fontSize: 25,
   paddingTop: 0,
   color: "black",
  //  marginBottom: 10,
  },
  textSubJudul: {
  //  paddingTop: 0,
   marginBottom: 10,
  },
  textIsi: {
    color: "black",
  },
  text: {
    color: "black",
  },
  parent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button: {
    width: '50%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
class HalamanHome extends React.Component {
  render() {
    return (
      <>
      <View style={styles.container}>
        <Image style={styles.logo} source={require('./logo_binar.png')}/>
      </View>
      <View style={styles.container2}>
        <Text style={styles.textJudul}>Styling di React Native</Text>
        <Text style={styles.textSubJudul}>Binar Academy - React Native</Text>
        <Text style={styles.textIsi}>As a component grows in complexity, It is much cleaner and efficient to use StyleSheet.create so as to define several styles in one place. </Text>
        {/* <View style={{flexDirection: 'row', justifyContent: 'space-around', flex: 4, marginTop: 20, marginLeft: 0}}> */}
        <View style={styles.parent}>
          <View style={styles.button}>
            <Text style={{color:'blue'}}>Understood!</Text>
          </View>
          <View style={styles.button}>
            <Text style={{color:'blue'}}>What?!!!</Text>
          </View>
          
        </View>
        
      </View>
      </>
    )
  }
}
export default HalamanHome